
//  main.cpp
//  test
//
//  Created by Aakanksha Reddy on 04/09/18.
//  Copyright © 2018 Aakanksha Reddy. All rights reserved.



/**#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define N 10

int main()
{
    
    int a[N];
    int b[N];
    int c[N];
    
    int chunk=3;
    int i;
    for(int j=0;j<N;j++)
    {
        a[j]=j*2;
        b[j]=j+5;
    }
    int totalThreads;
    int tid;
#pragma omp parallel shared(a,b,c,totalThreads,chunk) private(i,tid)
    {
        tid=omp_get_thread_num();
        if(tid==0)
        {
            totalThreads=omp_get_num_threads();
            printf("Total threads running are %d\n",totalThreads);
        }
        printf("ThREAD %d starting... \n",tid);
#pragma omp for schedule(static,chunk)
        for(i=0;i<N;i++){
            c[i]=a[i]+b[i];
            printf("Thread %d is running:c[%d]=%d\n",tid,i,c[i]);
        }
        printf("Thread %d is done. \n",tid);
    }
    
    return 0;
}
**/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define N 10

int main()
{

    int a[N];
    int b[N];
    int c[N];

    int i;
    for(int j=0;j<N;j++)
    {
        a[j]=j*2;
        b[j]=j+5;
    }
    int totalThreads;
    int tid;

#pragma omp parallel shared(a,b,c,totalThreads) private(i,tid)
    {
        tid=omp_get_thread_num();
        if(tid==0)
        {
            totalThreads=omp_get_num_threads();
            printf("Total threads running are %d\n",totalThreads);
        }
        printf("ThREAD %d starting... \n",tid);
#pragma omp for schedule(dynamic)
        for(i=0;i<N;i++){
            c[i]=a[i]+b[i];
            printf("Thread %d is running:c[%d]=%d\n",tid,i,c[i]);
        }
        printf("Thread %d is done. \n",tid);
    }

    return 0;
}




